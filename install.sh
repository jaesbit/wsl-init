#!/bin/bash

#copy our files repository

#Startup script enables ssh and crontab services
sudo cp bin/startup /bin/startup
sudo chmod o-w /bin/startup
sudo chmod g-w /bin/startup

# allow to run startup without password
echo "$USER    ALL=(root) NOPASSWD: /bin/startup" | sudo tee -a /etc/sudoers

cp -r wsl-terminal /mnt/c/
cp -r home/.tmux* $HOME
cp -r home/.exports $HOME

sudo apt update 

# install rust-lang 
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

#add cargo to path temporally
export PATH=$HOME/.cargo/env:$PATH

#dependencies to builds
sudo apt install build-essential -y

# install our common rust binaries
cargo install ripgrep find-files 


######
# Install nodejs 12 
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt install nodejs -y

sudo apt remove vim -y
sudo apt install neovim -y

/mnt/c/Windows/explorer.exe shell:startup
/mnt/c/Windows/explorer.exe /mnt/c/wsl-terminal

echo source $HOME/.exports >> $HOME/.bashrc

echo INSTALL DONE. 
echo please Move bash_init to startup